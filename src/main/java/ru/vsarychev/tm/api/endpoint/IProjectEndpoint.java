package ru.vsarychev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Project;

public interface IProjectEndpoint {

    @Nullable
    @GetMapping("/{id}")
    Project get(@PathVariable("id") @Nullable String id);

    @PostMapping
    void post(@RequestBody @Nullable Project project);

    @PutMapping
    void put(@RequestBody @Nullable Project project);

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") @Nullable String id);
}
