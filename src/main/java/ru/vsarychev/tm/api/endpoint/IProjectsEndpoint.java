package ru.vsarychev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Project;

import java.util.List;

public interface IProjectsEndpoint {
    @Nullable
    @GetMapping
    List<Project> get();

    @PostMapping
    void post(@RequestBody @Nullable List<Project> projects);

    @PutMapping
    void put(@RequestBody @Nullable List<Project> projects);

    @DeleteMapping
    void delete();
}
