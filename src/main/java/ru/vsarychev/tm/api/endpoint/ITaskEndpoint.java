package ru.vsarychev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Task;

public interface ITaskEndpoint {
    @Nullable
    @GetMapping("/{id}")
    Task get(@PathVariable("id") @Nullable String id);

    @PostMapping
    void post(@RequestBody @Nullable Task task);

    @PutMapping
    void put(@RequestBody @Nullable Task task);

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") @Nullable String id);
}
