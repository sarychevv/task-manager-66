package ru.vsarychev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.vsarychev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @Nullable
    Project findById(@Nullable String id);

    @Transactional
    void create(@Nullable Project project);

    @Transactional
    void update(@Nullable Project project);

    @Transactional
    void deleteById(@Nullable String id);

    @Nullable
    List<Project> findAll();

    @Transactional
    void saveAll(@Nullable List<Project> projects);

    @Transactional
    void removeAll();
}
