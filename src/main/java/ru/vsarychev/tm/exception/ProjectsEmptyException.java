package ru.vsarychev.tm.exception;

public final class ProjectsEmptyException extends AbstractException {

    public ProjectsEmptyException() {
        super("Error! Projects is empty...");
    }

}
