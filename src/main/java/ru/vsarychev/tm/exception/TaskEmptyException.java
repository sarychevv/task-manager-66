package ru.vsarychev.tm.exception;

public final class TaskEmptyException extends AbstractException {

    public TaskEmptyException() {
        super("Error! Task is empty...");
    }

}
