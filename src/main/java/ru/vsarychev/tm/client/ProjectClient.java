package ru.vsarychev.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Project;

public interface ProjectClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/project";

    static ProjectClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, BASE_URL);
    }

    @GetMapping("/{id}")
    Project get(@PathVariable("id") @Nullable String id);

    @PostMapping
    void post(@RequestBody @Nullable Project project);

    @PutMapping
    void put(@RequestBody @Nullable Project project);

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") @Nullable String id);

}
